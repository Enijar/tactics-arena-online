<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="author" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <title>Tactics Arena Online</title>
        <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    </head>

    <body ondragstart="return false;">
        <div id="wrapper">
            <div id="container">

                @yield('content')

            </div>
        </div>

        @include('partials.scripts')

        @yield('script')
    </body>
</html>