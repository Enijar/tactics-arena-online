@extends('layout.default')

@section('content')
    <div id="arena"></div>
@endsection

@section('script')
    @parent

    <script type="text/jsx">
        var tiles = JSON.parse('{!! json_encode($user->units) !!}');

        React.render(<Arena grid={tiles} />, document.getElementById('arena'));
    </script>
@endsection