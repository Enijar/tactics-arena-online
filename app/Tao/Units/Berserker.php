<?php

namespace App\Tao\Units;

class Berserker extends Unit
{
    public function __construct()
    {
        $this->name = 'Berserker';
        $this->hp = 42;
        $this->power = $this->getPower('stun', 22, true);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(25, 12, 0);
        $this->recovery = 1;
        $this->movement = $this->getMovement('normal', 3);

        return $this;
    }
}
