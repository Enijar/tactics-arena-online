<?php

namespace App\Tao\Units;

class DragonTyrant extends Unit
{
    public function __construct()
    {
        $this->name = 'Dragon Tyrant';
        $this->hp = 68;
        $this->power = $this->getPower('damage', 28, false);
        $this->attack = 1;
        $this->armor = 16;
        $this->blocking = $this->getBlocking(40, 20, 0);
        $this->recovery = 3;
        $this->movement = $this->getMovement('teleport', 4);

        return $this;
    }
}
