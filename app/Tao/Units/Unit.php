<?php

namespace App\Tao\Units;

use StdClass;

class Unit
{
    protected function getPower($type, $amount, $blockable)
    {
        $power = new StdClass();

        $power->type = $type;
        $power->amount = $amount;
        $power->blockable = $blockable;

        return $power;
    }

    protected function getBlocking($front, $side, $back)
    {
        $blocking = new StdClass();

        $blocking->front = $front;
        $blocking->side = $side;
        $blocking->back = $back;

        return $blocking;
    }

    protected function getMovement($type, $amount)
    {
        $movement = new StdClass();

        $movement->type = $type;
        $movement->amount = $amount;

        return $movement;
    }
}