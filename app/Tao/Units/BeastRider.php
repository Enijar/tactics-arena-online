<?php

namespace App\Tao\Units;

class BeastRider extends Unit
{
    public function __construct()
    {
        $this->name = 'Beast Rider';
        $this->hp = 38;
        $this->power = $this->getPower('damage', 19, true);
        $this->attack = 1;
        $this->armor = 15;
        $this->blocking = $this->getBlocking(45, 22, 0);
        $this->recovery = 1;
        $this->movement = $this->getMovement('normal', 4);

        return $this;
    }
}
