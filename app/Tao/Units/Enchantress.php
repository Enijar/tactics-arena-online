<?php

namespace App\Tao\Units;

class Enchantress extends Unit
{
    public function __construct()
    {
        $this->name = 'Enchantress';
        $this->hp = 35;
        $this->power = $this->getPower('paralyze', 0, false);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(0, 0, 0);
        $this->recovery = 3;
        $this->movement = $this->getMovement('normal', 3);

        return $this;
    }
}
