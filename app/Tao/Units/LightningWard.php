<?php

namespace App\Tao\Units;

class LightningWard extends Unit
{
    public function __construct()
    {
        $this->name = 'Lightening Ward';
        $this->hp = 56;
        $this->power = $this->getPower('damage', 30, false);
        $this->attack = 1;
        $this->armor = 18;
        $this->blocking = $this->getBlocking(100, 100, 100);
        $this->recovery = 4;
        $this->movement = $this->getMovement('immovable', 0);

        return $this;
    }
}
