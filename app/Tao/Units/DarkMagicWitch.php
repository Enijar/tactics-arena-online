<?php

namespace App\Tao\Units;

class DarkMagicWitch extends Unit
{
    public function __construct()
    {
        $this->name = 'Dark Magic Witch';
        $this->hp = 28;
        $this->power = $this->getPower('damage', 24, false);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(0, 10, 0);
        $this->recovery = 3;
        $this->movement = $this->getMovement('normal', 3);

        return $this;
    }
}
