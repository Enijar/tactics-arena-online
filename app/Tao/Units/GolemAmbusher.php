<?php

namespace App\Tao\Units;

class GolemAmbusher extends Unit
{
    public function __construct()
    {
        $this->name = 'Golem Ambusher';
        $this->hp = 60;
        $this->power = $this->getPower('damage', 20, true);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(0, 0, 0);
        $this->recovery = 3;
        $this->movement = $this->getMovement('normal', 2);

        return $this;
    }
}
