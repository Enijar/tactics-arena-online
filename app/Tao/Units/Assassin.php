<?php

namespace App\Tao\Units;

class Assassin extends Unit
{
    public function __construct()
    {
        $this->name = 'Assassin';
        $this->hp = 35;
        $this->power = $this->getPower('damage', 18, true);
        $this->attack = 1;
        $this->armor = 12;
        $this->blocking = $this->getBlocking(70, 35, 0);
        $this->recovery = 5;
        $this->movement = $this->getMovement('normal', 3);

        return $this;
    }
}
