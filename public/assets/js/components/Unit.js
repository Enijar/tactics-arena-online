var Unit = React.createClass({
    getUnitName: function () {
        if (!this.props.unit) {
            return '';
        }

        return this.props.unit.name.split(' ').join('-').toLowerCase()
    },
    render: function () {
        var unit = '';

        if (this.props.unit) {
            unit = React.createElement('img', {src: 'assets/img/units/' + this.getUnitName() + '.gif'});
        }

        return (
            <div className="unit">
                {unit}
            </div>
        );
    }
});